const path = require('path');
const fs = require('fs');

const {randomElements} = require('./utils');

function readQuestions(dir, countElements){
    const questionsData = fs.readdirSync(dir).map(file => readQuestion(path.join(dir, file)));
    const randomQuestions = randomElements(questionsData, countElements);
    return randomQuestions;
}

function readQuestion(file){
    const data = fs.readFileSync(file, 'utf8').split('\r\n');
    return {
        questionText: data[0],
        correctAnswer: Number(data[1])-1,
        answers: data.filter((value, index) => index>1)
    };
}

module.exports = {
    readQuestion,
    readQuestions
};