const readlineSync = require('readline-sync');

function quizItem(questionText, answers){
    const answer = readlineSync.keyInSelect(answers, questionText);
    if(answer === -1){
        if(readlineSync.keyInYN('Прервать тестирование?')){
            process.exit();
        }
    }
    return answer; 
}

function beginAgain(){
    return readlineSync.keyInYN('Пройти тест заново?');
}

module.exports = {
    quizItem,
    beginAgain
}