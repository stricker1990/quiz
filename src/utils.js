function randomFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomArrayElement(arr){
    if(arr.length === 0){
        return null;
    }
    const randomIndex = randomFromInterval(0, arr.length-1);
    return arr[randomIndex];
}

function randomElements(arr, countElements){
    if(countElements>arr.length){
        countElements=arr.length;
    }

    const result = [];

    for(let i=0; i<countElements; i++){
        const randomIndex = randomFromInterval(0, arr.length - 1);
        result.push(arr[randomIndex]);
        arr=arr.filter((element, index) => index !== randomIndex);
    }

    return result;
}

module.exports = {
    randomFromInterval,
    randomArrayElement,
    randomElements
}
