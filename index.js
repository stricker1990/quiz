const {readQuestions} = require('./src/files');

const {quizItem, beginAgain} = require('./src/dialogs');

function initQuestions(){
    try{
        return readQuestions('./res/questions', 5);
    }catch(e){
        console.error(`Ошибка при чтении файлов с вопросами: ${e}`);
        return null;
    };
}

function startQuiz(questions){

    let correctAnswers = 0;

    questions.forEach(question => {
        const answer = quizItem(question.questionText, question.answers);
        if(answer === question.correctAnswer){
            correctAnswers++;
            console.log(`Это правильный ответ!`);
        }else{
            console.log(`Это неправильный ответ :(`);
        }
    });

    console.log(`Правильных ответов: ${correctAnswers} из ${questions.length}`);
}

while(true){

    const questions = initQuestions();

    if(questions !== null){
        
        startQuiz(questions);
    }

    if(!beginAgain()){
        break;
    }


};